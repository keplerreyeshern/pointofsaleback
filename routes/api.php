<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\DataController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProviderController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\SaleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/admin/data', [DataController::class, 'admin']);
    Route::get('/public/data', [DataController::class, 'public']);

    //Users
    Route::resource('/users', UserController::class);

    // Companies
    Route::resource('/companies', CompanyController::class);
    Route::post('/companies/update/{company}', [CompanyController::class, 'update']);

    // Providers
    Route::resource('/providers', ProviderController::class);
    Route::post('/providers/update/{provider}', [ProviderController::class, 'update']);

    // Products
    Route::resource('/products', ProductController::class);
    Route::post('/products/update/{product}', [ProductController::class, 'update']);
    Route::post('/products/buy/{product}', [ProductController::class, 'buy']);

    // Sale
    Route::resource('/sale', SaleController::class);
    Route::get('/sale/create/{type}', [SaleController::class, 'create']);
});



