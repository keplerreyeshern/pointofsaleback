<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kepler Reyes Hernandez',
            'email' => 'keplerreyeshern@gmail.com',
            'password' => Hash::make('Balam.0412'),
            'profile' => 'super_admin',
            'company_id' => '1',
        ]);
        User::create([
            'name' => 'Balam Reyes Ochoa',
            'email' => 'balam@gmail.com',
            'password' => Hash::make('Balam.0412'),
            'profile' => 'super_admin',
            'company_id' => '1',
        ]);
    }
}
