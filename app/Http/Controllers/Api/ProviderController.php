<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Provider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::findOrFail(auth()->user()->company_id);
        $provider= new Provider();
        $provider->name = $request['name'];
        $provider->slug = Str::slug($request['name']);
        $provider->description = $request['description'];
        $provider->company_id = $company->id;
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/'.$company->name.'/providers/' . $name,  \File::get($file));
            $provider->image = '/storage/images/'.$company->name.'/providers/' . $name;
        }
        $provider->active = true;
        $provider->save();
        return json_encode($provider);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        if ($provider->active){
            $provider->active = false;
        } else {
            $provider->active = true;
        }
        $provider->save();
        return json_encode($provider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        $company = Company::findOrFail(auth()->user()->company_id);
        $provider->name = $request['name'];
        $provider->slug = Str::slug($request['name']);
        $provider->description = $request['description'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/'.$company->name.'/providers/' . $name,  \File::get($file));
            $provider->image = '/storage/images/'.$company->name.'/providers/' . $name;
        }
        $provider->active = true;
        $provider->save();
        return json_encode($provider);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        $provider->delete();
        return json_encode($provider);
    }
}
