<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ProductSale;
use App\Models\Sale;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if($user->profile == 'admin'){
            $salesPartial = Sale::whereDate('created_at', '=', date_create('now'))
                ->where('company_id', $user->company_id)
                ->where('active', true)
                ->get();
            $salesTotal = Sale::whereDate('created_at', '=', date_create('now'))
                ->where('company_id', $user->company_id)
                ->where('activeTotal', true)
                ->get();
        } else {
            $salesPartial = Sale::whereDate('created_at', '=', date_create('now'))
                ->where('company_id', $user->company_id)
                ->where('user_id', $user->id)
                ->where('active', true)
                ->get();
            $salesTotal = Sale::whereDate('created_at', '=', date_create('now'))
                ->where('company_id', $user->company_id)
                ->where('user_id', $user->id)
                ->where('activeTotal', true)
                ->get();
        }
        $result = [
            'salesPartial' => $salesPartial,
            'salesTotal' => $salesTotal,
        ];
        return json_encode($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $user = auth()->user();
        if ($type == 'partial'){
            if($user->profile == 'admin'){
                $sales = Sale::whereDate('created_at', '=', date_create('now'))
                    ->where('company_id', $user->company_id)
                    ->where('active', true)
                    ->get();
            } else {
                $sales = Sale::whereDate('created_at', '=', date_create('now'))
                    ->where('company_id', $user->company_id)
                    ->where('user_id', $user->id)
                    ->where('active', true)
                    ->get();
            }
            foreach($sales as $sale){
                $sale->active = false;
                $sale->save();
            }
        } else {
            $sales = Sale::whereDate('created_at', '=', date_create('now'))
                ->where('company_id', $user->company_id)
                ->where('activeTotal', true)
                ->get();
            foreach($sales as $sale){
                $sale->activeTotal = false;
                $sale->save();
            }
        }

        return json_encode($sales);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $sale = new Sale();
        $productsSelect = json_decode($request['sale'], true);
        $sale->total = $request['total'];
        $sale->user_id = $user->id;
        $sale->company_id = $user->company_id;
        $sale->active = true;
        $sale->save();
        foreach ($productsSelect as $product){
            $productSale = new ProductSale();
            $productSale->sale_id = $sale->id;
            $productSale->product_id = $product['id'];
            $productSale->amount = $product['amount'];
            $productSale->save();
        }
        $result = [
            'sale' => $sale
        ];
        return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
