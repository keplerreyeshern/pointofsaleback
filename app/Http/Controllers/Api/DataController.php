<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;
use App\Models\Provider;
use App\Models\User;
use Illuminate\Http\Request;

class DataController extends Controller
{
    function admin()
    {
        $users = User::all();
        $companies = Company::get();
        $result = [
            'users' => $users,
            'companies' => $companies
        ];
        return json_encode($result);
    }

    function public()
    {
        $user = auth()->user();
        $users = User::where('company_id', $user->company_id)->get();
        $providers = Provider::where('company_id', $user->company_id)->get();
        $products = Product::where('company_id', $user->company_id)->get();
        $result = [
            'users' => $users,
            'providers' => $providers,
            'products' => $products
        ];
        return json_encode($result);
    }
}
