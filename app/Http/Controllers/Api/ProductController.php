<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::findOrFail(auth()->user()->company_id);
        $product= new Product();
        $product->name = $request['name'];
        $product->code = $request['code'];
        $product->amount = $request['amount'];
        $product->slug = Str::slug($request['name']);
        $product->description = $request['description'];
        $product->price = $request['price'];
        $product->provider_id = $request['provider'];
        $product->company_id = $company->id;
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/'.$company->name.'/products/' . $name,  \File::get($file));
            $product->image = '/storage/images/'.$company->name.'/products/' . $name;
        }
        $product->active = true;
        $product->save();
        return json_encode($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if ($product->active){
            $product->active = false;
        } else {
            $product->active = true;
        }
        $product->save();
        return json_encode($product);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function buy(Request $request, Product $product)
    {
        $product->amount = $request['amount'];
        $product->save();
        return json_encode($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $company = Company::findOrFail(auth()->user()->company_id);
        $product->name = $request['name'];
        $product->slug = Str::slug($request['name']);
        $product->code = $request['code'];
        $product->description = $request['description'];
        $product->price = $request['price'];
        $product->provider_id = $request['provider'];
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/'.$company->name.'/products/' . $name,  \File::get($file));
            $product->image = '/storage/images/'.$company->name.'/products/' . $name;
        }
        $product->save();
        return json_encode($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return json_encode($product);
    }
}
